



function getCarForId(inventory, id) {

    if(!inventory ||  !id)
    {
        return "Invalid Argument Passed";
    }

    for(let i=0;i<inventory.length;i++)
    {
        
        if(inventory[i].id===id)
        {
            return `Car ${id} is a ${inventory[id].car_year} ${inventory[id].car_model}`;
        }
    }

    return "No car matching for the given ID";
    
}


module.exports = getCarForId;