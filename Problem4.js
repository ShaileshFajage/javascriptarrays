

function getCarYears(inventory) {

    if(!inventory)
    {
        return "Invalid Argument Passed";
    }
    
    var car_years = [];
    for(let i=0;i<inventory.length;i++)
    {
        car_years[i] = inventory[i].car_year;
    }

    return car_years;
}



module.exports = getCarYears;

