

function getLastCar(inventory) {

    if(!inventory)
    {
        return "Invalid Argument Passed";
    }
    
    return `Last car is a ${inventory[inventory.length-1].car_year} ${inventory[inventory.length-1].car_model}`
    //return 0;
}

module.exports = getLastCar;