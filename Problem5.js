var car = require(`./Problem4.js`)
const inventory = require(`./CarDetails`);

var carYears = car(inventory);

console.log(carYears);

function carBeforeYear2000() {
    
    let count = 0;

    for(i=0;i<carYears.length;i++)
    {
        if(carYears[i]<2000)
        {
            count+=1;
        }
    }

    return count;
}

module.exports = carBeforeYear2000;