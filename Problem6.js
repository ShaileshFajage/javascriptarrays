
function BMWAndAudiCars(inventory) {

    if(!inventory)
    {
        return "Invalid Argument Passed";
    }
    
    const BMWAndAudi = [];
    let j=0;

    for(let i=0;i<inventory.length;i++)
    {
        if(inventory[i].car_make=="BMW" || inventory[i].car_make=="Audi")
        {
            BMWAndAudi[j] = inventory[i].car_make;
            j++;
        }
    }

    return BMWAndAudi;
}

module.exports = BMWAndAudiCars;